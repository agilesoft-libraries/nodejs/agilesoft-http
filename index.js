"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.doRequest = exports.hasValidStatus = exports.Method = void 0;
const logger_1 = require("agilesoft-logger/logger");
const context_logger_1 = require("agilesoft-logger/context/context.logger");
const axios = __importStar(require("axios"));
const redisUtil = __importStar(require("agilesoft-redis/index"));
const internalLogger = logger_1.createInstance("agilesoft-http");
const DEFAULT_TIMEOUT_VALUE = 10000;
class Method {
}
exports.Method = Method;
Method.GET = "get";
Method.POST = "post";
Method.DELETE = "delete";
Method.OPTIONS = "options";
Method.PUT = "put";
Method.PATCH = "patch";
Method.PURGE = "purge";
Method.LINK = "link";
Method.UNLINK = "unink";
Method.HEAD = "head";
const DEFAULT_EXPIRATION_SECONDS = Number(process.env.CACHE_EXPIRATION_SECONDS) || 600;
const hasValidStatus = (...response) => {
    for (const resp of response) {
        if (resp.status < 200 || resp.status > 299) {
            return false;
        }
    }
    return true;
};
exports.hasValidStatus = hasValidStatus;
const doRequest = (requestConfig, extra) => __awaiter(void 0, void 0, void 0, function* () {
    const initTime = new Date().getTime();
    let cache = null;
    const logger = extra && extra.logger ? extra.logger : internalLogger;
    if (extra && extra.cache) {
        cache = extra.cache;
    }
    if (cache && cache.enabled && String(process.env.TEST_MODE) !== "true") {
        logger.debug(`Se verifica si existe información en cache para el servicio ${requestConfig.url} y key ${cache.key}`);
        const cacheData = yield redisUtil.getData(cache.key);
        if (cacheData) {
            logger.debug(`Se obtiene data desde cache para servicio ${requestConfig.url} y key ${cache.key}`, { data: cacheData });
            const cachedResponse = JSON.parse(cacheData).jsonData;
            cachedResponse.deltaTime = new Date().getTime() - initTime;
            return cachedResponse;
        }
        logger.debug(`No existe informacion en cache para servicio ${requestConfig.url} y key ${cache.key}`);
    }
    const serviceResponse = yield doHttpRequest(requestConfig, extra);
    if (cache && cache.enabled && String(process.env.TEST_MODE) !== "true") {
        if (serviceResponse.status >= 200 &&
            serviceResponse.status <= 299 &&
            serviceResponse.data) {
            logger.debug(`Se procede a almacenar en cache informacion del servicio ${requestConfig.url} y key ${cache.key}`, { data: serviceResponse.data });
            yield redisUtil.saveData({
                key: cache.key,
                data: JSON.stringify({ jsonData: serviceResponse }),
                expirationSeconds: cache.expirationSeconds || DEFAULT_EXPIRATION_SECONDS,
            });
        }
    }
    return serviceResponse;
});
exports.doRequest = doRequest;
const doHttpRequest = (requestConfig, extra) => __awaiter(void 0, void 0, void 0, function* () {
    const logger = extra && extra.logger ? extra.logger : internalLogger;
    const initTime = new Date().getTime();
    let responseCode;
    let responseStatus;
    let deltaTime;
    if (requestConfig.trxId == null) {
        requestConfig.trxId = String(initTime);
    }
    let axiosResponse;
    let timeOutParam = process.env.DEFAULT_TIMEOUT || DEFAULT_TIMEOUT_VALUE;
    let restOptions = requestConfig.options;
    let loggerHeaders = {};
    if (logger instanceof context_logger_1.ContextLogger) {
        const datosLogger = logger.getContext();
        loggerHeaders = {
            trxid: datosLogger.data.transaction.trxId,
            timestamp: datosLogger.data.transaction.timestamp,
            context: datosLogger.data.transaction.context,
            module: datosLogger.data.transaction.module,
            service: datosLogger.data.transaction.service,
            action: datosLogger.data.transaction.action,
            sessionid: datosLogger.data.client.sessionID,
            userid: datosLogger.data.client.userID,
            secondaryUserid: datosLogger.data.client.secondaryUserID,
            executiveid: datosLogger.data.client.executiveID,
        };
    }
    let reqHeaders = Object.assign(Object.assign({}, requestConfig.headers), loggerHeaders);
    if (requestConfig.options && requestConfig.options.timeout) {
        timeOutParam = requestConfig.options.timeout;
        delete restOptions.timeout;
    }
    if (requestConfig.queryParams) {
        let initQueryParam = false;
        requestConfig.url.includes("?")
            ? (initQueryParam = true)
            : (requestConfig.url = requestConfig.url + "?");
        Object.entries(requestConfig.queryParams).forEach(([key, value]) => {
            if (value) {
                requestConfig.url = `${requestConfig.url}${initQueryParam ? "&" : ""}${key}=${value}`;
                initQueryParam = true;
            }
        });
    }
    try {
        logger.debug(`Se realizará petición HTTP a servicio ${requestConfig.url}`);
        axiosResponse = yield axios.default.request(Object.assign({ url: requestConfig.url, method: requestConfig.method ? requestConfig.method : Method.GET, headers: reqHeaders, data: requestConfig.data, timeout: timeOutParam }, restOptions));
        responseCode = axiosResponse === null || axiosResponse === void 0 ? void 0 : axiosResponse.status;
        responseStatus = axiosResponse === null || axiosResponse === void 0 ? void 0 : axiosResponse.statusText;
    }
    catch (err) {
        axiosResponse = err.response;
        responseCode = (axiosResponse === null || axiosResponse === void 0 ? void 0 : axiosResponse.status) || 500;
        responseStatus = (axiosResponse === null || axiosResponse === void 0 ? void 0 : axiosResponse.statusText) || "TIMEOUT";
        logger.error(`Error al realizar petición HTTP a servicio ${requestConfig.url}, codigo error ${responseCode}`, err);
    }
    finally {
        const finalTime = new Date().getTime();
        deltaTime = finalTime - initTime;
    }
    logger.debug(`Tiempo de respuesta del servicio ${requestConfig.url} en ${deltaTime} ms`);
    return {
        deltaTime,
        data: axiosResponse === null || axiosResponse === void 0 ? void 0 : axiosResponse.data,
        headers: axiosResponse === null || axiosResponse === void 0 ? void 0 : axiosResponse.headers,
        status: responseCode,
        statusText: responseStatus,
        trxId: requestConfig === null || requestConfig === void 0 ? void 0 : requestConfig.trxId,
    };
});
//# sourceMappingURL=index.js.map