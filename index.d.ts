import { Logger } from "agilesoft-logger/logger";
export declare type HttpMethod = "get" | "GET" | "delete" | "DELETE" | "head" | "HEAD" | "options" | "OPTIONS" | "post" | "POST" | "put" | "PUT" | "patch" | "PATCH" | "purge" | "PURGE" | "link" | "LINK" | "unlink" | "UNLINK";
export declare class Method {
    static readonly GET = "get";
    static readonly POST = "post";
    static readonly DELETE = "delete";
    static readonly OPTIONS = "options";
    static readonly PUT = "put";
    static readonly PATCH = "patch";
    static readonly PURGE = "purge";
    static readonly LINK = "link";
    static readonly UNLINK = "unink";
    static readonly HEAD = "head";
}
export interface RequestData {
    url: string;
    method?: HttpMethod;
    headers?: any;
    data?: any;
    options?: any;
    trxId?: string;
    queryParams?: Object;
}
export interface ResponseData<T> {
    data: T;
    status: number;
    statusText: string;
    headers: any;
    trxId: string;
    deltaTime: number;
}
export interface CacheDef {
    enabled: boolean;
    key: string;
    expirationSeconds: number;
}
export interface RetryDef {
    quantity: number;
}
export interface ExtraOptions {
    cache?: CacheDef;
    logger?: Logger;
    retry?: RetryDef;
}
export declare const hasValidStatus: (...response: ResponseData<any>[]) => boolean;
export declare const doRequest: <T>(requestConfig: RequestData, extra?: ExtraOptions | undefined) => Promise<ResponseData<T>>;
